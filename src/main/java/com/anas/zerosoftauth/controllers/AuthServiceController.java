package com.anas.zerosoftauth.controllers;

import com.anas.zerosoftauth.config.DbService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class AuthServiceController {
    private static final Logger log = LoggerFactory.getLogger(AuthServiceController.class);

    @Autowired
    private DbService dbService;

    @PostMapping("/login")
    public String login(@RequestBody Object user) {
        log.info("<User = {}>, called login method", user.toString());
        String response = dbService.login(user);
        log.info("<User = {}>, login call finished", user.toString());
        return response;
    }

    @PostMapping("sign-up")
    public String signUp(@RequestBody Object user) {
        log.info("New registration request");
        String response = dbService.signUp(user);
        log.info("Registration completed successfully");
        return response;
    }
}
