package com.anas.zerosoftauth.config;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("db-service")
public interface DbService {
    @PostMapping("/login")
    public String login(Object creds);

    @PostMapping("/users/sign-up")
    public String signUp(Object user);
}
